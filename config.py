
DEBUG = False

FORECAST_TIME = 72
TIME_DELTA = 3

FTP = "ftp.ncep.noaa.gov"
FTP_FOLDER = "pub/data/nccf/com/gens/prod"

RESOLUTION = 'pgrb2a'
RESOLUTION = 'pgrb2ap5'

ENSEMBLE_SIZE = 20

DESTINATION_FOLDER = '/tmp'
