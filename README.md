# README #

this simple code download automaticaly the GFS ensemble and control



## Usage  ##
If you want download the ensemble for 12 of febrary of 2016 and 0 as analysis time 
you should be use program like that:



```
#!bash

gfs.py 20160212-00
```


## config.py ##

In the config file you can change some parameters

like 


```
#!python


FORECAST_TIME = 72
TIME_DELTA = 3

FTP = "ftp.ncep.noaa.gov"
FTP_FOLDER = "pub/data/nccf/com/gens/prod"

RESOLUTION = 'pgrb2a'   # 1 degre
RESOLUTION = 'pgrb2ap5' # 0.5 degre

ENSEMBLE_SIZE = 20

DESTINATION_FOLDER = '/tmp'
```