#!/usr/bin/python

import config
import time
from datetime import datetime, timedelta

def download_ensemble(anl_date):
    """
    get gfs ensemble forcing from ncep server ussing curl using experimentWRF
    parameters
    ftp://ftp.ncep.noaa.gov/pub/data/nccf/com/gens/prod/gefs.20160115/[00 06 12 18]/pgrb2a/

    """
    import os
    from ftplib import FTP

    end_date = anl_date + timedelta(hours = config.FORECAST_TIME)

    ftp = FTP(config.FTP)
    ftp.login()
    ftp.cwd( config.FTP_FOLDER )

    day_folder = "gefs."+anl_date.strftime("%Y%m%d/%H").zfill(2)+'/'+config.RESOLUTION
    ftp.cwd( day_folder )

    os.chdir(config.DESTINATION_FOLDER)

    for forecast_time in range(0, config.FORECAST_TIME+1, config.TIME_DELTA):

        gfs_file = "gec00.t%sz.pgrb2a.0p50.f%s" % \
        (anl_date.strftime("%H").zfill(2), str(forecast_time).zfill(3))

        ftp.retrbinary('RETR %s' % gfs_file, open(gfs_file, 'wb').write)

    for member in range(config.ENSEMBLE_SIZE + 1):
        for forecast_time in range(0, config.FORECAST_TIME+1, config.TIME_DELTA):

            gfs_file =  "gep%s.t%sz.pgrb2a.0p50.f%s" % \
            (str(member).zfill(2), anl_date.strftime("%H").zfill(2), \
            str(forecast_time).zfill(3) )

            ftp.retrbinary('RETR %s' % gfs_file, open(gfs_file, 'wb').write)


    return




if __name__ == "__main__":
    import argparse

    def mkdate(datestr):
        return datetime.strptime(datestr, '%Y%m%d-%H')

    parser = argparse.ArgumentParser()
    parser.add_argument('anl_date',type=mkdate)


    parser.add_argument('--debug', action='store_true', default=False)

    args = parser.parse_args()

    config.DEBUG = args.debug

    download_ensemble(args.anl_date)
